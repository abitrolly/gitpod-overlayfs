FROM gitpod/workspace-base

RUN pwd
RUN ls -la /
# It appears `/workspace` dir already exists in the base image
#RUN mkdir /workspace
RUN ls -la /workspace
RUN echo "File from Dockerfile layer" > /workspace/file_from_dockerfile

RUN mkdir /workspace/dir_from_dockerfile
RUN echo "Dir from Dockerfile layer." > /workspace/dir_from_dockerfile/README.md

RUN echo "HOME file from Dockerfile" > $HOME/file_from_dockerfile
