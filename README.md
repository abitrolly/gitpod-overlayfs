# gitpod-overlayfs

Dir listing of Gitpod `/workspace` layout.

```bash
gitpod /workspace/gitpod-overlayfs (main) $ ls -la ..
total 0
drwxr-xr-x 7 gitpod gitpod 102 Nov 16 15:35 .
drwxr-xr-x 1 root   root   110 Nov 16 15:35 ..
drwxr-xr-x 3 gitpod gitpod  17 Nov 16 15:35 .cargo
drwxr-xr-x 2 gitpod gitpod  19 Nov 16 15:35 .gitpod
drwxr-xr-x 3 gitpod gitpod  61 Nov 16 15:35 gitpod-overlayfs
drwxr-xr-x 4 gitpod gitpod  34 Nov 16 15:35 .pyenv_mirror
drwxr-xr-x 4 gitpod gitpod  36 Nov 16 15:35 .vscode-remote
```

The problem is that you can not preload files there with
Dockerfile, and this repo exists to prove that point.

To validate, open this repo in Gitpod.

https://gitpod.io/#https://gitlab.com/abitrolly/gitpod-overlayfs

Check `.gitpod.Dockerfile` for files that it creates, and
look if they are here.

```sh
$ cat /workspace/file_from_dockerfile
cat: /workspace/file_from_dockerfile: No such file or directory
```

```sh
$ cat $HOME/file_from_dockerfile
HOME file from Dockerfile
```

Interesting that now the `/workspace` content is different.

```sh
$ ls -la /workspace
total 0
drwxr-xr-x 5 gitpod gitpod  67 Nov 16 20:36 .
drwxr-xr-x 1 root   root   110 Nov 16 20:36 ..
drwxr-xr-x 2 gitpod gitpod  19 Nov 16 20:36 .gitpod
drwxr-xr-x 3 gitpod gitpod  80 Nov 16 20:36 gitpod-overlayfs
drwx------ 4 gitpod gitpod  36 Nov 16 20:36 .vscode-remote
```